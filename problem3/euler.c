#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void problem1();
void problem2();
void problem3();
void problem4();
void problem5();
void problem6();
void problem7();
void problem8();
void problem9();
void problem10();
void problem11();
void problem12();

int main()
{
	printf("***********************\nproject euler\n***********************\n");
	problem1();
	problem2();
	problem3();
	problem4();
	problem5();
	problem6();
	problem7();
 	problem8();
	problem9();
	problem10();
	problem11();
	problem12();
}


int is_prime(long long num)
{
	if (num == 2)
		return 1;

	// check for even numbers (!= 2)
	if (num % 2 == 0 || num <= 1)
		return 0;

	// iterate thru { 3, 5, 7, ..., sqrt(num) }, because we've already checked for even numbers (!= 2), which aren't prime
	long i = 3L, square_root = sqrtl(num);
	for (;i <= square_root;i += 2)
		if (num % i == 0)
			return 0;

	return 1;
}

long get_previous_prime(long num)
{
	for(--num;num > 1;--num)
		if (is_prime(num) == 1)
			return num;

	return -1L;
}

long get_next_prime(long num)
{
	while(is_prime(++num) == 0);
	return num;
}

int get_num_digits(int num)
{
	int i, num_digits = 0;
	for(i = num;i != 0; i /= 10)
		++num_digits;
	return num_digits;
}

int is_palindrome(int num)
{
	// initialize a char array the size of the # of digits
	int x, num_digits = get_num_digits(num);
	char str[num_digits + 1];

	// build string (don't forget to terminate with \0)
	str[num_digits] = 0;
	for(x = 0;x < num_digits;++x, num /= 10)
		str[num_digits - x - 1] = '0' + (num % 10);

	// set p to the beginning, and q to the end of the string
	char *q = str, *p = str;
	while(q && *q) ++q;

	// walk p & q toward the center of the string, comparing each char to determine if palindrome
	for(--q;p < q;++p, --q)		
		if (*q != *p)			
			return 0;

	return 1;
}

void problem1()
{
	int i, sum = 0;
	for(i = 0;i < 1000;++i)
		if (i % 3 == 0 || i % 5 == 0)
			sum += i;

	printf("problem 01: the sum of all numbers divisible by 3 or 5 up to 1,000: %d\n", sum);
}

void problem2()
{
	int i = 0, j = 1, t;
	unsigned long sum = 0;
	while(j < 4000000)
	{
		if ((j & 1) == 0)
			sum += j;
		t = j;
		j += i;
		i = t;
	}

	printf("problem 02: the sum of all even fibonacci numbers less than 4,000,000: %lu\n", sum);
}

void problem3()
{
	long long num = 600851475143LL;
	long i = sqrtl(num);
	for(;i > 1;i = get_previous_prime(i))
		if (num % i == 0)
			break;

	printf("problem 03: largest prime factor of 600,851,475,143: %ld\n", i);
}

void problem4()
{
	int min = 100;
	int max = 999;
	int i, j, greatest_palindrome = -1;
	for (i = min;i < max;++i)
		for(j = min;j < max;++j)
			if (is_palindrome(i * j) == 1 && greatest_palindrome < i * j)
				greatest_palindrome = i * j;

	printf("problem 04: largest palindrome product of two 3-digit numbers: %d\n", greatest_palindrome);
}

void problem5()
{
	int i;
	for (i = 1;
	  !(i % 20 == 0 &&
		i % 19 == 0 &&
		i % 18 == 0 &&
		i % 17 == 0 &&
		i % 16 == 0 &&
		i % 15 == 0 &&
		i % 14 == 0 &&
		i % 13 == 0 &&
		i % 12 == 0 &&
		i % 11 == 0);++i);

	printf("problem 05: smallest integer divisible by 1, 2, 3, ..., and 20: %d\n", i);
}

void problem6()
{
	unsigned int sum_of_squares = 0;
	int i = 1;
	for (;i < 101;++i)
		sum_of_squares += pow(i, 2);

	printf("problem 06: difference between (sum of squares) and (square of sums) for { 1, ..., 100 }: %d\n", (int)pow((100 * 101) / 2, 2) - sum_of_squares);
}

void problem7()
{
	int i = 0, p = 0;
	for(i = 0;i < 10001;i++)
		p = get_next_prime(p);

	printf("problem 07: 10,001st prime: %d\n", p);
}

int char_to_int(char c)
{
	return c - '0';
}

void problem8()
{
	char str[] = 
	"7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";

	int i, num = 0;
	int a, b, c, d, e;
	for(i = 0;i + 5 < (int)strlen(str);++i)
	{
		a = char_to_int(str[i]);
		b = char_to_int(str[i + 1]); 
		c = char_to_int(str[i + 2]);
		d = char_to_int(str[i + 3]);
		e = char_to_int(str[i + 4]);
		if (a * b * c * d * e > num)
			num = a * b * c * d * e;
	}

	printf("problem 08: greatest product of 5 consecutive integers within huge string: %d\n", num);
}



void problem10()
{
	int i, p = 0;
	unsigned long result = 0L;
	for (p = 2;p < 2000000;p = get_next_prime(p))
		result += p;

	printf("problem 10: sum of all primes less than 2,000,000: %lu\n", result);
}

void problem9()
{
	// set Euclid's formula variables (found via the commented code below)
	int m = 4, n = 1, k = 25;

	// use Euclid's formula to generate pythagorean triple whose sum is 1,000
	int a = k * (pow(m, 2) - pow(n, 2));
	int b = k * (2 * m * n);
	int c = k * (pow(m, 2) + pow(n, 2));

	if (a + b + c == 1000)
	{
		printf("problem 09: product of pythagorean triplet whose sum is 1,000: %d * %d * %d = %d\n", a, b, c, a * b * c);
	}
	/*
	int found = 0;
	for(n = 1;n < 2 && found == 0;++n)
		for(m = 1;m < 5 && found == 0;++m)
			if (m > n && gcd(m, n) == 1)
			{	
				// use Euclid's formula to generate pythagorean triples
				k = 25;	// optimized, k used to be in a 3rd loop
				a = k * (pow(m, 2) - pow(n, 2));
				b = k * (2 * m * n);
				c = k * (pow(m, 2) + pow(n, 2));

				if ((a + b + c) == 1000)
				{
					printf("problem 09: pythagorean triplet whose sum is 1,000: %d * %d * %d = %d (m: %d, n: %d, k: %d)\n", a, b, c, a * b * c, m, n, k);
					found = 1;
				}
			}
	*/
}

int gcd(int a, int b)
{
	int t;
	while (b != 0)
	{
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

	/*int grid[20][20] = { 
		{  8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8 },
		{ 49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0 },
		{ 81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65 },
		{ 52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56, 01, 32, 56, 71, 37,  2, 36, 91 },
		{ 22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80 },
		{ 24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50 },
		{ 32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70 },
		{ 67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21 },
		{ 24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72 },
		{ 21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95 }, 
		{ 78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92 },
		{ 16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57 },
		{ 86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58 },
		{ 19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40 },
		{  4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66 },
		{ 88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69 },
		{  4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36 },
		{ 20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16 },
		{ 20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54 },
		{  1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48 } };*/







int grid[20][20] = {
   	{ 8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8},
	{49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0},
	{81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65},
	{52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91},
	{22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80},
	{24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50},
	{32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70},
	{67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21},
	{24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72},
	{21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95},
	{78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92},
	{16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57},
	{86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58},
	{19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40},
	{ 4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66},
	{88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69},
	{ 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36},
	{20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16},
	{20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54},
	{ 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48} };
struct point
{
	int x;
	int y;
};

struct snake
{
	int level;
	struct point points[4];
};
int problem11_helper(int x, int y, int stepX, int stepY, int level)
{
	if (x < 0 || y < 0 || x > 19 || y > 19 || level == 4)
		return 1;

	return grid[x][y] * problem11_helper(x + stepX, y + stepY, stepX, stepY, level + 1);
}

void problem11()
{
	int i, j, result = 0;
	for(i = 0;i < 20;++i)
	{
		for(j = 0;j < 20;++j)
		{
			// compare horizontal & vertical #s
			int t = (int)fmax(problem11_helper(i, j, 0, 1, 0), problem11_helper(i, j, 1, 0, 0));

			// diagonal (down/left)
			t = (int)fmax(t, problem11_helper(i, j, 1, -1, 0));	

			// diagonal (down/right)
			t = (int)fmax(t, problem11_helper(i, j, 1, 1, 0));

			if (t > result)
				result = t;
		}
	}
	//struct point p = { 0, 6 };
	//struct snake s;
	// s.level = 0;
	// s.points[s.level] = p;
	// int x = get_largest_snake(s);
	// printf("%d\n", x);

	/*
	struct point p;
	struct snake s;
	for(i = 0;i < 20;++i)
	{
		for(j = 0;j < 20;++j)
		{
			p.x = i;
			p.y = j;
			s.level = 0;
			s.points[s.level] = p;
			int x = get_largest_snake(s);
			//printf("i: %d, j: %d ==> x: %d [1]:%d, [2]:%d\n", i, j, x, grid[s.points[0].x][s.points[0].y], grid[s.points[1].x][s.points[1].y]);
			if (x > result)
				result = x;
		}
	}
	*/
	
	printf("problem 11: greatest product of 4 adjacent numbers in 20x20 grid: %d\n", result);
}

int snake_has_already_been_to(struct snake s, int x, int y)
{
	int i;
	for (i = 0;i < 4;++i)
		if (s.points[i].x == x && s.points[i].y == y)
			return 1;
	return 0;
}

int get_largest_snake(struct snake s)
{
	if (s.level == 3)
	{	
		return grid[s.points[0].x][s.points[0].y] * grid[s.points[1].x][s.points[1].y] * grid[s.points[2].x][s.points[2].y] * grid[s.points[3].x][s.points[3].y]; 
	}
		
	// check for adjacent
	int offsetX, offsetY, newX, newY, max = 0;
	int a, b, c, d;
	struct snake xsnake;
	for(offsetX = -1;offsetX < 2;++offsetX)
	{
		for(offsetY = -1;offsetY < 2;++offsetY)
		{
			// get new point
			newX = s.points[s.level].x + offsetX;
			newY = s.points[s.level].y + offsetY;

			// guard against points we've already been to or points that are out of bounds
			if (snake_has_already_been_to(s, newX, newY) == 1 || newX < 0 || newX > 19 || newY < 0 || newY > 19)
				continue;

			// set new snake's level to the current level + 1
			xsnake.level = s.level + 1;

			// populate the new snake's previous and current points
			int i;
			for (i = 0;i < xsnake.level;++i)
				xsnake.points[i].x = s.points[i].x,
				xsnake.points[i].y = s.points[i].y;
			xsnake.points[xsnake.level].x = newX;
			xsnake.points[xsnake.level].y = newY;

			// recurse, and retain the maximum product found
			int result = get_largest_snake(xsnake);
			if (result > max) 
				max = result;
		}
	}
	return max;
}

int number_of_divisors(int num)
{
	int i, result = 0, rt = sqrtl(num);
	for(i = 1;i <= rt;++i)
		if (num % i == 0)
			++result;

	// every factor less than the square root is paired with a factor greater than the square root, so double the result
	return result << 1;
}

void problem12()
{
	int num = 1, i = 1;
	while (number_of_divisors(num) < 501)
		num += ++i;
	
	//if (number_of_divisors(9) == 3)
		printf("YAY!%d\n", number_of_divisors(9));

	printf("problem 12: first triangle number to have over 500 divisors: %d\n", num);
}
