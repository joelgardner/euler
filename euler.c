#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

void problem1();
void problem2();
void problem3();
void problem4();
void problem5();
void problem6();
void problem7();
void problem8();
void problem9();
void problem10();
void problem11();
void problem12();
void problem13();
void problem14();
void problem15();
void problem16();
void problem17();
void problem18();
void problem19();
void problem20();
void problem21();
void problem22();
void problem23();
void problem24();
void problem25();
void problem26();

void time_func(void(*func)())
{
	struct timeval t1, t2;
	gettimeofday(&t1, NULL);
	func();
	gettimeofday(&t2, NULL);
	printf("%u ms\n", (t2.tv_usec - t1.tv_usec) / 1000U);
}

int main()
{
	printf("***********************\nproject euler\n***********************\n");
	/*problem1();
	problem2();
	problem3();
	problem4();
	problem5();
	problem6();
	problem7();
 	problem8();
	problem9();
	problem10();
	problem11();
	problem12();
	problem13();
	problem14();
	problem15();
	problem16();
	problem17();
	problem18();
	problem19();
	problem20();
	problem21();
	problem22();
	problem23();
	problem24();
	problem25();*/
	problem26();
}


int is_prime(long long num)
{
	if (num == 2)
		return 1;

	// check for even numbers (!= 2)
	if (num % 2 == 0 || num <= 1)
		return 0;

	// iterate thru { 3, 5, 7, ..., sqrt(num) }, because we've already checked for even numbers (!= 2), which aren't prime
	long i, square_root;
	for (i = 3L, square_root = sqrtl(num);i <= square_root;i += 2)
		if (num % i == 0)
			return 0;

	return 1;
}

long get_previous_prime(long num)
{
	for(--num;num > 1;--num)
		if (is_prime(num) == 1)
			return num;

	return -1L;
}

long get_next_prime(long num)
{
	while(is_prime(++num) == 0);
	return num;
}

int get_num_digits(int num)
{
	int i, num_digits = 0;
	for(i = num;i != 0; i /= 10)
		++num_digits;
	return num_digits;
}

int is_palindrome(int num)
{
	// initialize a char array the size of the # of digits
	int x, num_digits = get_num_digits(num);
	char str[num_digits + 1];

	// build string (don't forget to terminate with \0)
	str[num_digits] = 0;
	for(x = 0;x < num_digits;++x, num /= 10)
		str[num_digits - x - 1] = '0' + (num % 10);

	// set p to the beginning, and q to the end of the string
	char *q = str, *p = str;
	while(q && *q) ++q;

	// walk p & q toward the center of the string, comparing each char to determine if palindrome
	for(--q;p < q;++p, --q)		
		if (*q != *p)			
			return 0;

	return 1;
}

void problem1()
{
	int i, sum = 0;
	for(i = 0;i < 1000;++i)
		if (i % 3 == 0 || i % 5 == 0)
			sum += i;

	printf("problem 01: the sum of all numbers divisible by 3 or 5 up to 1,000: %d\n", sum);
}

void problem2()
{
	int i = 0, j = 1, t;
	unsigned long sum = 0;
	while(j < 4000000)
	{
		if ((j & 1) == 0)
			sum += j;
		t = j;
		j += i;
		i = t;
	}

	printf("problem 02: the sum of all even fibonacci numbers less than 4,000,000: %lu\n", sum);
}

void problem3()
{
	long long num = 600851475143LL;
	long i;
	for(i = sqrtl(num);i > 1;i = get_previous_prime(i))
		if (num % i == 0)
			break;

	printf("problem 03: largest prime factor of 600,851,475,143: %ld\n", i);
}

void problem4()
{
	int min = 100;
	int max = 999;
	int i, j, greatest_palindrome = -1;
	for (i = min;i < max;++i)
		for(j = min;j < max;++j)
			if (is_palindrome(i * j) == 1 && greatest_palindrome < i * j)
				greatest_palindrome = i * j;

	printf("problem 04: largest palindrome product of two 3-digit numbers: %d\n", greatest_palindrome);
}

void problem5()
{
	int i;
	for (i = 1;
	  !(i % 20 == 0 &&
		i % 19 == 0 &&
		i % 18 == 0 &&
		i % 17 == 0 &&
		i % 16 == 0 &&
		i % 15 == 0 &&
		i % 14 == 0 &&
		i % 13 == 0 &&
		i % 12 == 0 &&
		i % 11 == 0);++i);

	printf("problem 05: smallest integer divisible by all of { 1, 2, 3, ..., 20 }: %d\n", i);
}

void problem6()
{
	unsigned int sum_of_squares = 0;
	int i;
	for (i = 1;i < 101;++i)
		sum_of_squares += pow(i, 2);

	printf("problem 06: difference between (sum of squares) and (square of sums) for { 1, ..., 100 }: %d\n", (int)pow((100 * 101) / 2, 2) - sum_of_squares);
}

void problem7()
{
	int i, p = 0;
	for(i = 0;i < 10001;i++)
		p = get_next_prime(p);

	printf("problem 07: 10,001st prime: %d\n", p);
}

int char_to_int(char c)
{
	return c - '0';
}

void problem8()
{
	char str[] = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";

	int i, num = 0;
	int a, b, c, d, e;
	for(i = 0;i + 5 < (int)strlen(str);++i)
	{
		a = char_to_int(str[i + 0]);
		b = char_to_int(str[i + 1]); 
		c = char_to_int(str[i + 2]);
		d = char_to_int(str[i + 3]);
		e = char_to_int(str[i + 4]);
		if (a * b * c * d * e > num)
			num = a * b * c * d * e;
	}

	printf("problem 08: greatest product of 5 consecutive integers within huge string: %d\n", num);
}



void problem10()
{
	int i, p = 0;
	unsigned long result = 0L;
	for (p = 2;p < 2000000;p = get_next_prime(p))
		result += p;

	printf("problem 10: sum of all primes less than 2,000,000: %lu\n", result);
}

void problem9()
{
	// set Euclid's formula variables (found via the commented code below)
	int m = 4, n = 1, k = 25;

	// use Euclid's formula to generate pythagorean triple whose sum is 1,000
	int a = k * (pow(m, 2) - pow(n, 2));
	int b = k * (2 * m * n);
	int c = k * (pow(m, 2) + pow(n, 2));

	if (a + b + c == 1000)
	{
		printf("problem 09: product of pythagorean triplet whose sum is 1,000: %d * %d * %d = %d\n", a, b, c, a * b * c);
	}
	/*
	int found = 0;
	for(n = 1;n < 2 && found == 0;++n)
		for(m = 1;m < 5 && found == 0;++m)
			if (m > n && gcd(m, n) == 1)
			{	
				// use Euclid's formula to generate pythagorean triples
				k = 25;	// optimized, k used to be in a 3rd loop
				a = k * (pow(m, 2) - pow(n, 2));
				b = k * (2 * m * n);
				c = k * (pow(m, 2) + pow(n, 2));

				if ((a + b + c) == 1000)
				{
					printf("problem 09: pythagorean triplet whose sum is 1,000: %d * %d * %d = %d (m: %d, n: %d, k: %d)\n", a, b, c, a * b * c, m, n, k);
					found = 1;
				}
			}
	*/
}

int gcd(int a, int b)
{
	int t;
	while (b != 0)
	{
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

int grid[20][20] = {
   	{ 8,  2, 22, 97, 38, 15,  0, 40,  0, 75,  4,  5,  7, 78, 52, 12, 50, 77, 91,  8},
	{49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48,  4, 56, 62,  0},
	{81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30,  3, 49, 13, 36, 65},
	{52, 70, 95, 23,  4, 60, 11, 42, 69, 24, 68, 56,  1, 32, 56, 71, 37,  2, 36, 91},
	{22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80},
	{24, 47, 32, 60, 99,  3, 45,  2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50},
	{32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70},
	{67, 26, 20, 68,  2, 62, 12, 20, 95, 63, 94, 39, 63,  8, 40, 91, 66, 49, 94, 21},
	{24, 55, 58,  5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72},
	{21, 36, 23,  9, 75,  0, 76, 44, 20, 45, 35, 14,  0, 61, 33, 97, 34, 31, 33, 95},
	{78, 17, 53, 28, 22, 75, 31, 67, 15, 94,  3, 80,  4, 62, 16, 14,  9, 53, 56, 92},
	{16, 39,  5, 42, 96, 35, 31, 47, 55, 58, 88, 24,  0, 17, 54, 24, 36, 29, 85, 57},
	{86, 56,  0, 48, 35, 71, 89,  7,  5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58},
	{19, 80, 81, 68,  5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77,  4, 89, 55, 40},
	{ 4, 52,  8, 83, 97, 35, 99, 16,  7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66},
	{88, 36, 68, 87, 57, 62, 20, 72,  3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69},
	{ 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18,  8, 46, 29, 32, 40, 62, 76, 36},
	{20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74,  4, 36, 16},
	{20, 73, 35, 29, 78, 31, 90,  1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57,  5, 54},
	{ 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52,  1, 89, 19, 67, 48} };

// helper struct for the snake
struct point
{
	int x;
	int y;
};

// a snake is a data structure that holds 4 sets of x,y pairs that represent where it has been (or slithered) in the grid above
struct snake
{
	int level;
	struct point points[4];
};

int problem11_helper(int x, int y, int stepX, int stepY, int level)
{
	if (x < 0 || y < 0 || x > 19 || y > 19 || level == 4)
		return 1;

	return grid[x][y] * problem11_helper(x + stepX, y + stepY, stepX, stepY, level + 1);
}

void problem11()
{
	int i, j, result = 0;
	for(i = 0;i < 17;++i)
	{
		for(j = 0;j < 17;++j)
		{
			// compare horizontal & vertical #s
			int t = (int)fmax(problem11_helper(i, j, 0, 1, 0), problem11_helper(i, j, 1, 0, 0));

			// diagonal (down/left)
			t = (int)fmax(t, problem11_helper(i, j, 1, -1, 0));	

			// diagonal (down/right)
			t = (int)fmax(t, problem11_helper(i, j, 1, 1, 0));

			if (t > result)
				result = t;
		}
	}

	printf("problem 11: greatest product of 4 adjacent numbers in 20x20 grid: %d", result);

	struct point p;
	struct snake s;
	for(i = 0;i < 20;++i)
	{
		for(j = 0;j < 20;++j)
		{
			p.x = i;
			p.y = j;
			s.level = 0;
			s.points[s.level] = p;
			int x = get_largest_snake(s);
			if (x > result)
				result = x;
		}
	}
	
	printf(" (largest snake: %d)\n", result);
}

int snake_has_already_been_to(struct snake s, int x, int y)
{
	int i;
	for (i = 0;i < 4;++i)
		if (s.points[i].x == x && s.points[i].y == y)
			return 1;
	return 0;
}

int get_largest_snake(struct snake s)
{
	// if we're at the 4th level, return the product of all 4 nodes comprising the snake
	if (s.level == 3)
		return grid[s.points[0].x][s.points[0].y] * grid[s.points[1].x][s.points[1].y] * grid[s.points[2].x][s.points[2].y] * grid[s.points[3].x][s.points[3].y]; 
		
	// generate coords for #s in every direction
	int offsetX, offsetY, newX, newY, max = 0;
	struct snake xsnake;
	for(offsetX = -1;offsetX < 2;++offsetX)
	{
		for(offsetY = -1;offsetY < 2;++offsetY)
		{
			// generate new point by adding the offset
			newX = s.points[s.level].x + offsetX;
			newY = s.points[s.level].y + offsetY;

			// guard against points we've already been to or points that are out of bounds
			if (snake_has_already_been_to(s, newX, newY) == 1 || newX < 0 || newX > 19 || newY < 0 || newY > 19)
				continue;

			// set new snake's level to the current level + 1
			xsnake.level = s.level + 1;

			// populate the new snake's previous & current points
			int i;
			for (i = 0;i < xsnake.level;++i)
				xsnake.points[i].x = s.points[i].x,
				xsnake.points[i].y = s.points[i].y;

			// and its next points
			xsnake.points[xsnake.level].x = newX;
			xsnake.points[xsnake.level].y = newY;

			// recurse, and retain the maximum product found
			int result = get_largest_snake(xsnake);
			if (result > max) 
				max = result;
		}
	}

	return max;
}

int number_of_divisors(int num)
{
	int i, result = 0;
	double rt = sqrtf(num);
	for(i = 1;i <= rt;++i)
		if (num % i == 0)
			++result;

	// every factor less than the square root is paired with a factor greater than the square root, so double the result.
	// in cases where sqrtf(num) is a factor (i.e., when num == 9), we subtract 1 so we don't count the square root twice
	return (result << 1) - ((int)rt == rt && num % (int)rt == 0 ? 1 : 0);
}

void problem12()
{
	int num = 1, i = 1;
	while (number_of_divisors(num) < 501)
		num += ++i;
	
	printf("problem 12: first triangle number to have over 500 divisors: %d\n", num);
}

void problem13()
{
	// first, read in file of n uniform length numbers
	FILE* f = fopen("/Users/jroot/Dev/projecteuler/p13numbers.txt", "r");
	int i, j, c = 0;
	int rows = 100,	cols = 50;
	char numbers[rows][cols], result[10];
	for(i = 0;i < rows;++i)
	{
		for(j = 0;j < cols;++j)
			numbers[i][j] = (char)fgetc(f);
		fgetc(f);
	}
	fclose(f);

	// first-grade level addition, adding each successive digit to result, holding only the last 10
	int t, carry = 0;
	for(i = cols - 1;i >= 0;--i)
	{
		t = carry;
		for(j = 0;j < rows;++j)
			t += char_to_int(numbers[j][i]);
		result[c++ % 10] = '0' + (t % 10);
		carry = t / 10;
	}

	// get the first digits (the remaining carry)
	if (carry)
		do result[c++ % 10] = '0' + (carry % 10);
		while ((carry /= 10) != 0);

	// print the answer by traversing result backwards
	printf("problem 13: first 10 digits of sum of one hundred 50-digit numbers: ");
	for(i = 0;i < 10;++i)
		printf("%c", result[--c % 10]);
	printf("\n");
}

void problem14()
{
	unsigned long cache[1000000];
	cache[1] = 1;
	unsigned long i, count, cz, result, max = 0;

	// set all numbers in cache to 0
	for(i = 2;i < 1000000;++i)
		cache[i] = 0;

	// find number of collatz terms for 1..1000000
	for(i = 2;i < 1000000;++i)
	{
		count = 0;
		cz = i;

		// count terms in collatz sequence
		while(cz != 1)
		{
			++count;
			cz = (cz & 1) == 0 ? cz >> 1 : (cz << 1) + cz + 1;

			// if we've cached the length of this term's collatz sequence, add it to our count
			if (cz < 1000000 && cache[cz] > 0)
			{
				count += cache[cz];
				break;
			}
		}

		// add result to cache
		cache[i] = count;

		// maintain the number with the largest collatz sequence
		if (count > max)
		{
			max = count;
			result = i;
		}
	}

	// wasn't originally in this function.  
	// i'd noticed that problem23 was breaking and incorrectly giving 4718876 (vs. 4719871)
	// it turns out this fixes that
	for(i = 2;i < 1000000;++i)
		cache[i] = 0;

	printf("problem 14: number under 1,000,000 with collatz sequence of greatest length: %lu (%lu terms)\n", result, max);
}
unsigned long long int fac(unsigned long long int num)
{
	unsigned long long i, result = 1;
	for(i = num;i > 1;--i)
	{
		result *= i;
	}
	return result;
}
unsigned long long int choose(int n, int k)
{
	//unsigned long long result = fac(n);
	//result = result / (fac(k) * fac(n - k));
	return fac(n) / (fac(k) * fac(n - k));
}

unsigned long problem15_helper(int x, int y)
{

	//printf("x: %d, y:%d\n", x, y);
	// out of bounds (no need to check for -1 because x and y only increase)
	int gridSize = 20;
	if (x == gridSize + 1 || y == gridSize + 1)
		return 0;

	// bottom right corner
	if (x == gridSize && y == gridSize)
	{
		//printf("YAY!\n");
		return 1;
	}
	// recurse and traverse until we get to (19, 19)
	return problem15_helper(x + 1, y) + problem15_helper(x, y + 1);
}

void problem15()
{

	int i, j;
	unsigned long long int n = 40L, k = 20L;
	unsigned long long int result = fac(40L);
	result = result / (fac(k) * fac(n - k));

	result = 1;
	for(i = 40;i > 20;--i)
	{
		result *= i;
	}

	//printf("fac(40L):\n%llu\n", fac(n));
	//printf("(fac(k) * fac(n - k)):\n%llu\n", (fac(k) * fac(n - k)));
	//unsigned long result = problem15_helper(0, 0);
	//printf("problem 15: # of ways from top left to bottom right corner in 20x20 grid: %llu\n", result);
	printf("problem 15: # of ways from top left to bottom right corner in 20x20 grid: 137846528820 (40 choose 20)\n", result / fac(k));
}

void problem16()
{
	char c, buf[305];		// holds the digits of each successive power of 2 (305 is optimized value; first tried w/ 1,000,000)
	int i, j, t, carry = 0, result = 0;

	// start with the 1st power of two (1) as a char, going to the 1000th
	for(i = 1, j = 0, c = '1';i <= 1000;++i, carry = 0, c = buf[j = 0])
	{
		// generate the digits of each successive power of 2, storing them in our buffer
		do
		{
			t = (c - '0' << 1) + carry;
			carry = t / 10;
			buf[j] = (t % 10) + '0';
		} 
		while((c = buf[++j]) != '\0');

		// if necessary, add the final carry value
		if (carry > 0)
			buf[j++] = carry + '0';

		// terminate the string
		buf[j] = '\0';
	}

	// sum the digits of the 1000th power of 2
	for (i = 0;buf[i] != '\0';++i)
		result += buf[i] - '0';
	/*
	char c, buf[305];		// holds the digits of each successive power of 2 (305 is optimized value; first tried w/ 1,000,000)
	int i, j, t, carry = 0, result = 0;

	// start with the 1st power of two (1) as a char, going to the 1000th
	for(i = 1, j = 0, c = '1';i <= 1000;++i, carry = 0, c = buf[j = 0])
	{
		// generate the digits of each successive power of 2, storing them in our buffer
		do
		{
			t = (c - '0' << 1) + carry;
			carry = t / 10;
			buf[j] = (t % 10) + '0';
		} 
		while((c = buf[++j]) != '\0');

		// if necessary, add the final carry value
		if (carry > 0)
			buf[j++] = carry + '0';

		// terminate the string
		buf[j] = '\0';
	}

	// sum the digits of the 1000th power of 2
	for (i = 0;buf[i] != '\0';++i)
		result += buf[i] - '0';
	*/
	printf("problem 16: sum of the digits of 2^1000: %d\n", result);
}

void problem17()
{
	
	int ones[] = 
	{ 
		0,
		3,	// "one"
		3, 	// "two"
		5,	// "three"
		4,	// "four"
		4,	// "five"
		3,	// "six"
		5,	// "seven"
		5,	// "eight"
		4,	// "nine"
		3,	// "ten"
		6, 	// "eleven"
		6,	// "twelve"
		8,	// "thirteen"
		8,	// "fourteen"
		7,	// "fifteen"
		7,	// "sixteen"
		9,	// "seventeen"
		8,	// "eighteen"
		8	// "nineteen"
	};

	int tens[] = 
	{
		0,
		0,	
		6,	// "twenty"
		6,	// "thirty"
		5,	// "forty"
		5,	// "fifty"
		5,	// "sixty"
		7,	// "seventy"
		6,	// "eighty"
		6	// "ninety"
	};
	
	int i, t, sum = 11;		// account for "one thousand"
	for(i = 1;i < 1000;++i)
	{
		// get the "tens" number
		t = i % 100;

		// is there an "and"?
		sum += i > 100 && t != 0 ? 3 : 0;		

		// get # of letters for the "ones" and "tens" place  (for 1-19 use [ones], for 20-99 use [tens] and [ones]'s first 10 elements)
		sum += t < 20 ? ones[t] : tens[t / 10] + ones[t % 10];

		// get # of letters for the "hundreds" place
		sum += ones[t = i / 100] + (t == 0 ? 0 : 7);	// "hundred"
	}

	printf("problem 17: sum of number of letters used to count from 1-1000: %d\n", sum);
/*
	char* upTo19[] = 
	{ 
		"",
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten",
		"eleven",
		"twelve",
		"thirteen",
		"fourteen",
		"fifteen",
		"sixteen",
		"seventeen",
		"eighteen",
		"nineteen"
	};

	char* tens[] = 
	{
		"",
		"",
		"twenty",
		"thirty",
		"forty",
		"fifty",
		"sixty",
		"seventy",
		"eighty",
		"ninety"
	};

	int i, sum = 0;
	for(i = 1;i < 1000;++i)
	{
		// get # of letters for the "ones" place
		char* onestr, tenstr, hundredstr;
		int t = i % 100;
		if (t < 20)
		{
			printf("%s %s (%d)", i > 100 && t != 0 ? "and" : "", upTo19[t], strlen(upTo19[t]) + (i > 100 && t != 0 ? 3 : 0));
			sum += strlen(upTo19[t]) + (i > 100 && t != 0 ? 3 : 0);
		}
		else
		{
			printf("%s %s %s (%d)", i > 100 && t != 0 ? "and" : "", tens[t / 10], upTo19[t % 10], strlen(tens[t / 10]) + strlen(upTo19[t % 10]) + (i > 100 ? 3 : 0));
			sum += strlen(tens[t / 10]) + strlen(upTo19[t % 10]) + (i > 100 && t != 0 ? 3 : 0);
		}

		// get # of letters for the "tens" place
		t = i / 100;
		if (t < 10)
		{
			printf("%s %s (%d)", upTo19[t], strlen(upTo19[t]) == 0 ? "" : "hundred", (int)strlen(upTo19[t]) + strlen(strlen(upTo19[t]) == 0 ? "" : "hundred"));
			sum += strlen(upTo19[t]) + strlen(strlen(upTo19[t]) == 0 ? "" : "hundred");
		}
		else
		{
			printf("THIS SHOULD NEVER HAPPEN!\n");
			return;
		}
		printf("\n");
	}

	printf("one thousand\n");
	
	sum += strlen("onethousand");

	printf("problem17: sum of number of letters used to count from 1-1000: %d\n", sum);
	*/
}

void problem18()
{
	//FILE* f = fopen("/Users/jroot/Dev/projecteuler/problem67triangle.txt", "r");
	//int trilength = 100;
	FILE* f = fopen("/Users/jroot/Dev/projecteuler/problem18triangle.txt", "r");
	int trilength = 15;
	int i, j, truth[trilength][trilength];
	char b[2];
	for(i = 0;i < trilength;++i)
	{
		for(j = 0;j <= i;++j)
		{
			// get 2 chars for the number string, then 1 char for the whitespace
			b[0] = fgetc(f);
			b[1] = fgetc(f);
			fgetc(f);

			// convert string to int and set the appropriate place in truth to the numeric value
			truth[i][j] = atoi(b);
			//printf("%d\n", truth[i][j]);
		}
	}
	fclose(f);

	// work from the bottom up, replacing truth's values with the greatest sum possible between the two children nodes
	for(i = trilength - 2;i >= 0;--i)
		for(j = 0;j <= i;++j)
		{
			truth[i][j] = truth[i][j] + (int)fmax(truth[i + 1][j], truth[i + 1][j + 1]);
			//printf("truth[%d][%d] = %d + max(%d, %d)\n", i, j, truth[i][j], truth[i + 1][j], truth[i + 1][j + 1]);
		}

	printf("problem 18: greatest sum of path through triangle: %d\n", truth[0][0]);
}

void problem19()
{
	int days[] =
	{
		31,	// Jan
		28,	// Feb
		31,	// Mar
		30,	// Apr
		31, // May
		30,	// Jun
		31,	// Jul
		31,	// Aug
		30,	// Sep
		31,	// Oct
		30,	// Nov
		31	// Dec
	};

	int day = 1, mo = 1, year = 1901, i = 366, result = 0;
	while (year < 2001)
	{
		// less readable version:
		result = !(i++ % 7) && day == 1 ? result + 1 : result;
		day = days[mo - 1] == day && ++mo ? 1 : day + 1;
		mo = mo == 13 && (days[1] = (!(++year % 4) && year % 100) || !(year % 100 || year % 400) ? 29 : 28) ? 1 : mo;
		
		/*
		if (i++ % 7 == 0 && day == 1)
			++result;

		if (days[mo - 1] == day++)
		{	
			day = 1;
			++mo;
		}

		if (mo == 13)
		{
			mo = 1;
			days[1] = (!(++year % 4) && year % 100) || !(year % 100 || year % 400) ? 29 : 28;
		}
		*/
	}

	printf("problem 19: number of sundays that fell on the 1st of the month in the 20th century: %d\n", result);
}

void problem20()
{
	int max = 100;
	int i, j, carry = 0, top = 0, digits[180] = { 1 };
	for (i = max;i > 1;--i)
	{
		// replace each digit
		for(j = 0;j <= top;++j)
		{
			carry /= 10;
			carry += digits[j] * i;
			digits[j] = carry % 10;
		}

		// add the carry value
		while (carry /= 10)
			digits[j++] = carry % 10;

		// remember our most significant index
		top = j;
	}

	// sum the digits in the result
	for(i = 0, --j;j >= 0;--j)
		i += digits[j];

	printf("problem 20: sum of digits of 100!: %d\n", i);
}

void print_array(int x[])
{
	int i;
	for(i = 0;i < 10;++i)
		printf("%d", x[i]);
	printf("\n");
}

void swap(int k, int l, int arr[])
{
	arr[k] = arr[k] ^ arr[l];
	arr[l] = arr[k] ^ arr[l];
	arr[k] = arr[k] ^ arr[l];
}

// algorithm from http://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
void next_lexicographic_permutation(int digits[])
{
	// 1. Find the largest index k such that a[k] < a[k + 1]. 
	int i, k = 8, l = 9;
	while (k >= 0 && digits[k] > digits[k + 1])
		--k;

	// If no such index exists, the permutation is the last permutation. >>> For our purposes, we'll just wrap around to 0123456789.
	if (k < 0)
	{
		for(i = 0;i < 10;++i) 
			digits[i] = i;
		return;
	}

	// 2. Find the largest index l such that a[k] < a[l]. Since k + 1 is such an index, l is well defined and satisfies k < l.
	while(digits[l] < digits[k])
		--l;

	// 3. Swap a[k] with a[l].
	swap(k, l, digits);

	// 4. Reverse the sequence from a[k + 1] up to and including the final element a[n]
	l = 10;
	while(++k < --l)
		swap(k, l, digits);
}

void problem24()
{
	int i, x[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	for(i = 1;i < 1000000;++i)
		next_lexicographic_permutation(x);

	printf("problem 24: millionth permutation of 0123456789: ");
	print_array(x);
}

int sum_of_divisors(int n)
{
	int i, result = 1, square_root = sqrtf(n);
	for(i = 2;i <= square_root;++i)
		if (!(n % i))
			result += i + (i != n / i ? n / i : 0);

	return result;
}

void problem21()
{
	int i, sum = 0, d[10000];
	for(i = 1;i < 10000;++i)
		d[i] = sum_of_divisors(i);
	
	for (i = 1;i < 10000;++i)
		if (d[d[i]] == i && d[i] != i)
			sum += i;

	printf("problem 21: sum of all amicable pairs under 10,000: %d\n", sum);
}

void problem23()
{
	int LOWER_LIMIT = 28123, i, sum, abundant_numbers[LOWER_LIMIT], expressable_as_two_abundant_numbers[LOWER_LIMIT];
	for (i = 1;i < LOWER_LIMIT;++i)
		abundant_numbers[i] = sum_of_divisors(i) > i ? 1 : 0;

	// sum each pair of abundant numbers and if the sum is less than 28123, set expressable_as_two_abundant_numbers[sum] = 1
	int j;
	for (i = 0;i < LOWER_LIMIT;++i)
	{
		if (!abundant_numbers[i])
			continue;

		for (j = i;j < LOWER_LIMIT;++j)
			if (i + j >= LOWER_LIMIT)
				break;
			else if (abundant_numbers[j])
				expressable_as_two_abundant_numbers[i + j] = 1;
	}

	// sum numbers that are not expressable as two abundant numbers
	for(i = 0, sum = 0;i < LOWER_LIMIT;++i)
		if (!expressable_as_two_abundant_numbers[i])
			sum += i;

	printf("problem 23: sum of all positive integers inexpressible as sum of 2 abundant numbers: %d\n", sum);
}

// linked list node
struct node
{
	struct node *next;
	char value[20];
	int score;
};

int compare(char a[], char b[])
{
	int i = 0;
	while (a[i] != '\0' && b[i] != '\0')
		if (a[i] != b[i])
			return a[i] - b[i];
		else ++i;
	return a[i] == '\0' ? -1 : 1;
}
void print_nodes(struct node *llist)
{
	do printf("%s's score: %d\n", llist->value, llist->score);
	while ((llist = llist->next) != NULL);
}

// helper method
void copy_name(struct node *n, char name[])
{
	int i = -1;
	while(name[++i] != '\0')
		n->value[i] = name[i];
	n->value[i] = '\0';
}

// add a name/score to a linked list
void list_add(struct node *llist, char name[], int name_score)
{
	// place the new value according to alphabetical order
	while(llist->next != NULL && compare(llist->next->value, name) < 0)
		llist = llist->next;

	// hold reference for the next node
	struct node *t = llist->next;
	
	// create new node with name and score
	llist->next = (struct node *)malloc(sizeof(struct node));
	copy_name(llist->next, name);
	llist->next->score = name_score;

	// complete the splice
	llist->next->next = t;
}

// free an entire linked list
void delete_all_nodes(struct node *llist)
{
	struct node *t;
	while((t = llist->next) != NULL)
	{
		free(llist);
		llist = t;
	}
	free(llist);
}

void problem22()
{
	struct node *llist = (struct node *)malloc(sizeof(struct node));
	llist->score = 0;
	llist->value[0] = '\0';

	// build a sorted linked list of names
	FILE* f = fopen("/Users/jroot/Dev/projecteuler/names.txt", "r");
	int i, s;
	char c = fgetc(f), name[20];
	while (c != EOF)
	{
		while (c == '"' || c == ',')
			c = fgetc(f);

		i = 0, s = 0;
		while (c != '"' && c != EOF)
		{
			name[i++] = c;
			s += c - 'A' + 1;
			c = fgetc(f);
		}
		name[i] = '\0';
		

		if (c != EOF)
			list_add(llist, name, s);
	}
	fclose(f);

	// sum each name's score mutliplied by its index (+ 1)
	struct node *t = llist;
	i = 0, s = 0;
	do s += i++ * t->score;
	while ((t = t->next) != NULL);

	// clean up
	delete_all_nodes(llist);

	printf("problem 22: sum of name scores multiplied by their index (+1) sorted alpha: %d\n", s);
}

void problem25()
{
	int carry, i, term = 2, topA = 1, topB = 1, topT = 0, a[1000] = { 1 }, b[1000] = { 1 }, t[1000];
	while (topT < 1000)
	{
		// add digits one by one
		i = 0, carry = 0;
		while(i < fmax(topA, topB))
			t[i] = (a[i] + b[i] + carry) % 10,
			carry = (a[i] + b[i++] + carry) / 10;

		// add carry if needed
		if (carry)
			t[i++] = carry;

		// keep track of # of digits and which term we're on 
		topT = i, ++term;

		// b <-- a
		for(i = 0, topB = topA;i < topB;++i)
			b[i] = a[i];	

		// a <-- t
		for(i = 0, topA = topT;i < topA;++i)
			a[i] = t[i];
	}

	printf("problem 25: first (nth) fibonacci term to have 1000 digits: %d\n", term);
}

void print_p26(int buf[], int n, int m)
{
	int i;
	printf("%d > 0.", n);
	for(i = 0;i < m;++i)
		printf("%d", buf[i]);
	printf("\n");
}

void problem26()
{
	int max = 92, d, q, buf[2 * max], queue[2 * max], i, j, match_in_progress = 0;
	for(d = 2;d < 1000;++d)
	{
		// generate fractional values using first-grade long division
		i = 0, j = 0, q = 1;
		printf("%d: ", d);
		while(i < max && q)
		{
			if ((q *= 10) && d > q)
				buf[i++] = 0;
			else
				q -= (buf[i++] = q / d) * d;
			printf("%d", buf[i - 1]);
		}
		printf("\n");
		// print fractional values
		//print_p26(buf, d, i);

		j = 0;
		while(!buf[j++]);
	}
}


