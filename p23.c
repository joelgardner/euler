#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int sum_of_divisors(int n)
{
	int i, result = 1, square_root = sqrtf(n);
	for(i = 2;i <= square_root;++i)
		if (!(n % i))
			result += i + (i != n / i ? n / i : 0);

	return result;
}

void problem23()
{
	int LOWER_LIMIT = 28123;

	int i, sum, abundant_numbers[LOWER_LIMIT], expressable_as_two_abundant_numbers[LOWER_LIMIT];
	for (i = 1;i < LOWER_LIMIT;++i)
		abundant_numbers[i] = sum_of_divisors(i) > i ? 1 : 0;

	// initalize to all zeros
	for(i = 0;i < LOWER_LIMIT;++i) 
		expressable_as_two_abundant_numbers[i] = 0;

	// sum each pair of abundant numbers and if the sum is less than 28123, set expressable_as_two_abundant_numbers[sum] = 1
	int j;
	for (i = 0;i < LOWER_LIMIT;++i)
	{
		if (!abundant_numbers[i])
			continue;

		for (j = 0;j < LOWER_LIMIT;++j)
			if (i + j >= LOWER_LIMIT)
				break;
			else if (abundant_numbers[j])
				expressable_as_two_abundant_numbers[i + j] = 1;
	}

	// sum numbers that are not expressable as two abundant numbers
	for(i = 0, sum = 0;i < LOWER_LIMIT;++i)
		if (!expressable_as_two_abundant_numbers[i])
			sum += i;

	printf("problem23: sum of all positive integers inexpressible as sum of 2 abundant numbers: %d\n", sum);
}

int main()
{
	problem23();
}